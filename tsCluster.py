import pandas as pd
import datetime
import numpy as np
import os, glob
import matplotlib.pyplot as plt
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from dtaidistance import dtw
from statsmodels.robust import mad
import pywt


class tsCluster:
    def __init__(self):
        self.BasePath = os.path.dirname(__file__)
        self.TimeSeriesVert = os.path.join(self.BasePath, 'data_input', 'cbym338e2202-compl.csv')
        self.TimeSeriesStore = os.path.join(self.BasePath, 'data_output')

    def ts_clustering(self, df, day_start, hour_start, colID_subjID,
                      colTarget_Att, hours_window, restrict=False,
                      include_dynamic_time_warp=False):
        """
        Simple PCA clustering of TS. More complexities can follow, but this is for demonstration.
        :return:
        """
        # add weekday to ts
        df['weekday'] = df['datetime'].dt.dayofweek


        print(df.head())

        # align the subject to given day/time
        self.align_by_day_hour(df, day_start=day_start, hour_start=hour_start)

        # align the series by time for target attribute
        df = self.align_series(df, colID_subjID=colID_subjID,
                               colTarget_Att=colTarget_Att)

        subject_id_header = list(df)

        # define hours in epoch
        hours_window = hours_window

        # split stack align
        split_stacked_aligned = self.split_stack_by_hours(df, hours_window)
        print('Shape pre-zero empty drop', np.shape(split_stacked_aligned))

        subdivision, hours = np.shape(split_stacked_aligned)

        # bring into dataframe and drop things
        split_stacked_aligned_df = pd.DataFrame(split_stacked_aligned)
        split_stacked_aligned = split_stacked_aligned_df[(split_stacked_aligned_df.T != 0).any()]
        print('Shape post zero empty drop', np.shape(split_stacked_aligned))

        # calculate mean compliance by time definition
        split_stacked_aligned_mean = np.ravel(split_stacked_aligned.mean(axis=1))
        print('Length of stacked aligned mean', len(split_stacked_aligned_mean))

        # dtw (if applicable)
        # find dynamic time warp distance metric (returns as matrix)
        ## compare the distance to the mean to bin classes.
        restrict = False  # np.shape(split_stacked_aligned)[0]

        if restrict:
            split_stacked_aligned = split_stacked_aligned[:int(restrict)]

        if include_dynamic_time_warp:
            dtw_dist_mat = self.dynamic_time_warp_dist(split_stacked_aligned,
                                                            restict=restrict,
                                                            signal_comparator=split_stacked_aligned_mean)

            dtws = dtw.distance_matrix_fast(df)
            print(dtws)

        # calculate PCA for time series
            # combine dtw distance metric with other metrics, like PCA
            # add the dtw distance metrics to the the PCA results - don't worry, this is only one way
        split_stacked_aligned_PCA_dtw_train = split_stacked_aligned

        # split_stacked_aligned_PCA_dtw_train = np.c_[split_stacked_aligned, dtw_dist_mat]
        print(np.shape(split_stacked_aligned_PCA_dtw_train))

        ## calculate PCA for the raw split/stacked compliance data
        n_components = 2  # only take first two components
        split_stacked_aligned_PCA = PCA(n_components=n_components).fit_transform(split_stacked_aligned_PCA_dtw_train)
        print(np.shape(split_stacked_aligned_PCA_dtw_train))

        return split_stacked_aligned_PCA




    def hours_column2timestamp(self, data, colID_hours='hour', colID_date='date',
                               colID_combined='datetime'):
        """

        :param data:
        :param colID_hours:
        :param colID_date:
        :param colID_combined:
        :param weekday:
        :return:
        """
        data[colID_date] = data[colID_date].values.astype('datetime64[D]')
        data[colID_combined] = data[colID_date] + \
                               pd.to_timedelta(data[colID_hours], unit='h')

        return data

    def add_weekday(self, data):
        data['weekday'] = data['datetime'].dt.dayofweek

        return data

    def wma(self, signal, weights):
        """

        :param signal:
        :param weights:
        :return:
        """
        w_signal = np.convolve(np.ravel(signal), np.array(weights)[::-1], mode='same')
        return w_signal / np.sum(weights)

    def align_by_day_hour(self, data, colID_subjID='subjid',
                          day_start=0,
                          hour_start=0):
        """
        Aligns a TS to week/hour across subjects.
        time in time series.

        :param data:
        :param day_start:
        :param hour_start:
        :return:
        """

        unique_subjids = np.unique(data[colID_subjID])

        for subject in unique_subjids:
            # select data by subjectid
            subj_df = data.loc[data[colID_subjID] == subject]

            # remove all dates up to first occurance on the previous day
            first_occ_prior_day = subj_df['weekday'].values.searchsorted(day_start - 1,
                                                                         side='right')
            subj_df = subj_df[first_occ_prior_day:]

            # find the first occurance of weekday for target day
            first_occurance_weekday = subj_df['weekday'].values.searchsorted(day_start, side='right')
            subj_df = subj_df[first_occurance_weekday:]

            # find the first occurance of overlapping first time
            first_occurance_desired_hour = subj_df['hour'].values.searchsorted(hour_start, side='right')
            subj_df = subj_df[first_occurance_desired_hour:]

            subj_df.to_csv(os.path.join(self.TimeSeriesStore, str(str(subject) + '.csv')))

    def combine_ts(self, delimiter='/*.csv'):
        """

        :param delimiter:
        :param raw_all_batch:
        :return:
        """
        print('Combining data_input in a single .csv.')
        all_files_ = glob.glob(self.TimeSeriesStore + delimiter)
        list_ = []
        file_list_length = len(all_files_)

        track_it = 1

        for file_ in all_files_:
            print('Accessing file', track_it, 'of:', file_list_length)
            print('Reading file.')
            temp_df = pd.read_csv(file_, index_col=0)

            print('Appending file to file list.')
            list_.append(temp_df)
            track_it += 1

        return pd.concat(list_)

    def align_series(self, data,
                     colID_subjID='subjid',
                     colTarget_Att='adherence',
                     fillna_val=0.0):
        """
        Aligns the

        :param data:
        :param fillna_val:
        :param drop_cols:
        :return:
        """

        # align time series by patient
        aligned_series = {}
        for name, group in data.groupby(data[colID_subjID].values):
            aligned_series[str(name)] = pd.Series(group[colTarget_Att].values)

        # create single dataframe with aligned series
        aligned_data = pd.DataFrame(aligned_series)

        # replace NaNs as 0s .
        aligned_data.fillna(value=fillna_val, inplace=True)

        return aligned_data

    #
    def split_stack_by_hours(self, data, hours_window=168):

        # split the dataframe into series of 168 hours.
        mat_df = []
        last_ = int(np.floor(len(data)) // hours_window - 1)

        for g, df in data.groupby(np.arange(len(data)) // hours_window):
            df = df.transpose()

            if g < last_:
                mat_df.append(np.matrix(df))

        # vertically stack the data to get the weekly shapes.
        split_stacked_data = pd.DataFrame(np.vstack(mat_df))

        return split_stacked_data

    def dynamic_time_warp_dist(self, data, restict=300, signal_comparator=[]):
        """
        Perform dynamic time warping distance metric.
        :param data: 
        :param restict: 
        :param signal_comparator: 
        :return: 
        """
        print(data.head())

        ts_weeks, ts_hours = np.shape(data)
        if restict:
            ts_weeks = restict

        # convert to matrix and transpose
        data_mat = np.matrix(data)
        print('Shape of the data', np.shape(data_mat))

        # initialize a container for distances
        dtw_dist_mat = []
        for week in range(0, ts_weeks):
            week_data = np.ravel(data_mat[week, :])
            distance, path = fastdtw(week_data, signal_comparator, dist=euclidean)
            dtw_dist_mat.append(distance)

        return dtw_dist_mat

    def dynamic_time_warp_dist(self, data):
        """
        Perform dynamic time warping distance metric.
        :param data:
        :param restict:
        :param signal_comparator:
        :return:
        """

        return dtw.distance_matrix_fast(data)

    @staticmethod
    def wavelet_smooth(x, wavelet="coif1", level=1):
        # calculate the wavelet coefficients
        coeff = pywt.wavedec(x, wavelet, mode="symmetric", axis=0)

        # calculate a threshold
        sigma = mad(coeff[-level])

        # apply threshold
        uthresh = sigma * np.sqrt(1 * np.log(len(x)))
        coeff[1:] = (pywt.threshold(i, value=uthresh, mode="less") for i in coeff[1:])

        # reconstruct the signal using the thresholded coefficients
        return pywt.waverec(coeff, wavelet, mode="symmetric")

    def wma_df(self, df, i, filter_col_name, value_col_name, weight_func=[]):
        """

        :param df:
        :param i:
        :param filter_col_name:
        :param value_col_name:
        :param weight_func:
        :return:
        """
        df_ = df.copy(deep=True)

        filt_vals = df_[df_[filter_col_name] == i]
        filt_vals_ = filt_vals[value_col_name]
        filt_vals_date = np.array(filt_vals['datetime'])

        filt_vals_index = filt_vals_.index.values
        filt_vals_array = np.array(filt_vals_)
        weighted_signal = self.wma(filt_vals_array, weight_func)

        return filt_vals_index, filt_vals_date, weighted_signal

    @staticmethod
    def plot_decision_boundary(h=0.02, X=[], y=[], PCAd_data=[], n_clusters=3):
        """

        :param h: mesh step size (<1)
        :param X:
        :param y:
        :param PCAd_data:
        :param n_clusters:
        :return:
        """

        kmeans_c = KMeans(n_clusters=n_clusters).fit(PCAd_data)

        # Plot the decision boundary. For that, we will assign a color to each
        x_min, x_max = X.min() - 1, X.max() + 1
        y_min, y_max = y.min() - 1, y.max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        print(np.shape(xx), np.shape(yy))

        # Obtain labels for each point in mesh. Use last trained model.
        Z = kmeans_c.predict(np.c_[xx.ravel(), yy.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.figure(1)
        plt.clf()

        plt.imshow(Z, interpolation='nearest',
                   extent=(xx.min(), xx.max(), yy.min(), yy.max()),
                   cmap=plt.cm.Paired,
                   aspect='auto', origin='lower')

        plt.plot(X, y, 'k.', markersize=2)
        # Plot the centroids as a white X
        centroids = kmeans_c.cluster_centers_
        plt.scatter(centroids[:, 0], centroids[:, 1],
                    marker='x', s=169, linewidths=3,
                    color='w', zorder=10)
        plt.title('K-means clustering on the BYM dataset (PCA-reduced data)\n'
                  'Centroids are marked with white cross')
        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)
        plt.xticks(())
        plt.yticks(())
        plt.show()

    @staticmethod
    def update_graph_1(subj_id_uniques, data, plot_radio, subjid_drop, analys_check, slider_value, wavelet_slider_value, wavelet_type):
        subj_id_ = subjid_drop

        if 1 in plot_radio:
            subj_id_ = subj_id_uniques

        # assign data to a new variable name to prevent over-write
        plot_df = data

        if 'wma_denoise' in analys_check:
            """ wma denoising"""

            window_size = slider_value
            weights = np.ones(window_size)

            new_df = pd.DataFrame()
            for i in subj_id_:
                df = pd.DataFrame()
                wma_interim = tsCluster.wma_df(df=data, i=i, filter_col_name='subjid',
                                               value_col_name='adherence',
                                               weight_func=weights)
                df['adherence'] = wma_interim[2]
                df['subjid'] = [i] * len(wma_interim[2])
                df['datetime'] = wma_interim[1]

                new_df = pd.concat([new_df, df])

            plot_df = new_df

        if 'wd' in analys_check:
            """ wavelet denoising """

            df_ = plot_df.copy(deep=True)
            df_.dropna(inplace=True, axis=0)
            new_df = pd.DataFrame()
            for i in subj_id_:
                filt_vals = df_[df_['subjid'] == i]
                filt_vals_date = np.array(filt_vals['datetime']).ravel()
                filt_vals_ = np.array(filt_vals['adherence']).ravel()

                data_to_denoise = np.array(filt_vals_).ravel()
                denoised_signal = tsCluster.wavelet_smooth(data_to_denoise, wavelet=wavelet_type,
                                                           level=wavelet_slider_value)

                if len(denoised_signal) > len(data_to_denoise):
                    print(np.shape(filt_vals_), 'shape of signal to be denoised')
                    print(np.shape(filt_vals_date), 'shape of datetime ')
                    denoised_signal = denoised_signal[:len(data_to_denoise)]

                if len(denoised_signal) < len(data_to_denoise):
                    print(np.shape(filt_vals_), 'shape of signal to be denoised')
                    print(np.shape(filt_vals_date), 'shape of datetime ')
                    filt_vals_date = data_to_denoise[:len(denoised_signal)]

                print(np.shape(denoised_signal), 'shape of denoised signal')

                df = pd.DataFrame()
                df['adherence'] = denoised_signal
                df['subjid'] = [i] * len(denoised_signal)
                df['datetime'] = filt_vals_date

                new_df = pd.concat([new_df, df])

            plot_df = new_df

        return subj_id_, plot_df

