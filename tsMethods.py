import pandas as pd
import numpy as np
from tsCluster import tsCluster

# class instantiation
TClust = tsCluster()

# read in the data
df = pd.read_csv(TClust.TimeSeriesVert)

# correct the time series to provide a single timestamp
df = TClust.hours_column2timestamp(df)

TClust.ts_clustering(df=df, day_start=0, hour_start=0,
                     hours_window=24 * 4 * 2, colID_subjID='subjid',
                     colTarget_Att='adherence',
                     restrict=False, include_dynamic_time_warp=False
                     )




# drop the data nans
data = df.copy(deep=True)
data.drop(['date', 'visit'], inplace=True, axis=1)
print(np.shape(data))
data.dropna(inplace=True, axis=0)
print(np.shape(data))

# remove subjid's with less than 10 values
g = data.groupby('subjid')
data = g.filter(lambda x: len(x) > 10)

subj_id_uniques = np.unique(data['subjid'])
subj_id_uniques = list(map(int, subj_id_uniques))

# isolate individual subjects data
subj_id = subj_id_uniques[0]
subj_data = data[data['subjid'] == subj_id]

# Create list of subject IDs for dropdown
subjDropdownDictList = list()
for subj_id in subj_id_uniques:
    subjDropdownDictListInt = dict()
    subjDropdownDictListInt['label'] = str(subj_id)
    subjDropdownDictListInt['value'] = subj_id
    subjDropdownDictList.append(subjDropdownDictListInt)

subjDescribe = pd.DataFrame()
for subj_id in subj_id_uniques:
    desc = data[data['subjid'] == subj_id].describe()
    desc_out = pd.concat([subjDescribe, desc])




# data.drop(['hour', 'date', 'visit'], inplace=True, axis=1)
