import pandas as pd
import numpy as np
import pywt

from tsCluster import tsCluster
from plotly.offline import init_notebook_mode
from plotly.graph_objs import Layout, Scattergl, Contour, Scatter
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import dash_table_experiments as dt
from statsmodels.robust import mad
from sklearn.cluster import KMeans


# calculate total

def app_call():
    """
    Function wrapper to call layout below
    :return:
    """

    app.layout = html.Div([
        html.H1('BYM Timeseries Data Selection and Analysis'),
        # intro
        html.Div([
            html.Label('Subjects to analyze'),
            dcc.RadioItems(
                id='radio-button',
                options=[{'label': 'All Subjects', 'value': [1]},
                         {'label': 'Manual Selection', 'value': [0]}],
                value=[],
                labelStyle={'display': 'inline-block'})
        ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20, }),

        # dropdown
        html.Div([
            html.Label('Multi-Select Dropdown'),
            dcc.Dropdown(
                id='multi-dropdown',
                options=subjDropdownDictList,
                value=[],
                multi=True)
        ], style={'margin-top': 20, 'margin-bottom': 20, 'margin-left': 20, 'margin-right': 20, }),

        html.Div([

            # preprocessing checklist
            html.Div([
                html.Label('Preprocessing'),
                dcc.Checklist(
                    id='analysis-checklist',
                    options=[
                        {'label': 'Weighted moving average denoise', 'value': 'wma_denoise'},
                        {'label': 'Wavelet denoise', 'value': 'wd'}],
                    values=[],
                    labelStyle={'display': 'inline-block'})
            ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20, }),

            # wma slider
            html.Div([
                html.Label('Weighted moving average window'),
                dcc.Slider(
                    id='slider-updatemode-wma-window',
                    min=0,
                    max=24 * 7,
                    step=1,
                    value=3,
                    updatemode='drag',
                    marks={
                        0: '0 hr',
                        24: '1 day',
                        24 * 2: '2 days',
                        24 * 3: '3 days',
                        24 * 4: '4 days',
                        24 * 5: '5 days',
                        24 * 6: '6 days',
                        24 * 7: '1 week'}),
                html.Div(id='updatemode-output-container', style={'margin-top': 20})
            ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20, 'column-count':2}),

            # wavelet slider
            html.Div([
                html.Label('Wavelet Level'),
                dcc.Slider(
                    id='slider-updatemode-wavelet-level',
                    min=0,
                    max=24,
                    step=1,
                    value=3,
                    updatemode='drag'),
                html.Div(id='updatemode-output-container-wavelet-level', style={'margin-top': 20, 'margin-bottom': 20})
            ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20}),

            # wavelet function selection
            html.Div([
                dcc.RadioItems(
                    id='wavelet-function-radio',
                    options=[
                        {'label': 'haar', 'value': 'haar'},
                        {'label': 'dmeyr', 'value': 'dmey'},
                        {'label': 'db1', 'value': 'db1'},
                        {'label': 'db2', 'value': 'db2'},
                        {'label': 'db4', 'value': 'db4'},
                        {'label': 'coif1', 'value': 'coif1'},
                        {'label': 'coif4', 'value': 'coif4'},
                        {'label': 'bior1.1', 'value': 'bior1.1'},
                        {'label': 'rbio1.1', 'value': 'rbio1.1'}
                    ],
                    value=['haar'],
                    labelStyle={'display': 'inline-block'})
            ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20}),

            # clustering time-window slider
            html.Div([
                html.Label('Clustering Time Window'),
                dcc.Slider(
                    id='slider-updatemode-cluster-time-window',
                    min=24,  # 1 day
                    max=24 * 7 * 4 * 6,  # 6 months
                    step=24,
                    value=24 * 7,
                    updatemode='drag'),
                html.Div(id='updatemode-output-container-hours-window',
                         style={'margin-top': 20})
            ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20}),

            # n clusters slider
            html.Div([
                html.Label('Number of clusters Window'),
                dcc.Slider(
                    id='slider-updatemode-cluster-n-window',
                    min=2,  # 1 day
                    max=30,  # 6 months
                    step=1,
                    value=3,
                    updatemode='drag',
                ),
                html.Div(id='updatemode-output-container-n-cluster-window')
            ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20}),
        ], style={'columnCount': 1}),

        # graph object
        html.Div([
            html.Label('Time series:'),
            dcc.Graph(id='BYM-timeseries')
        ], style={'margin-top': 20, 'margin-left': 20, 'margin-right': 20}),
    ])

    # wma slider
    @app.callback(Output('updatemode-output-container', 'children'), [Input('slider-updatemode-wma-window', 'value')])
    def display_value(value):
        return 'Weighted moving average window {} hrs:'.format(value)

    # wavelet slider
    @app.callback(Output('updatemode-output-container-wavelet-level', 'children'),
                  [Input('slider-updatemode-wavelet-level', 'value')])
    def display_value(value):
        return 'Wavelet level {}:'.format(value)

    # # cluster time window slider
    # @app.callback(Output('updatemode-output-container-hours-window', 'children'),
    #               [Input('slider-updatemode-cluster-time-window', 'value')])
    # def display_value(value):
    #     return 'Clustering Hours {}:'.format(value)
    #
    # # cluster n slider
    # @app.callback(Output('updatemode-output-container-n-cluster-window', 'children'),
    #               [Input('slider-updatemode-cluster-n-window', 'value')])
    # def display_value(value):
    #     return 'Clustering Hours {}:'.format(value)

    #
    # # cluster day start slider (day of week [dow])
    # @app.callback(Output('updatemode-output-container-dow', 'children'),
    #               [Input('slider-updatemode-cluster-dow', 'value')])
    # def display_value(value):
    #     return 'Clustering Hours {}:'.format(value)
    #
    # # cluster hour start slider (time of day [tod])
    # @app.callback(Output('updatemode-output-container-tod', 'children'),
    #               [Input('slider-updatemode-cluster-tod', 'value')])
    # def display_value(value):
    #     return 'Time of day align: {}:'.format(value)

    @app.callback(Output('BYM-timeseries', 'figure'), [Input('radio-button', 'value'),
                                                       Input('multi-dropdown', 'value'),
                                                       Input('analysis-checklist', 'values'),
                                                       Input('slider-updatemode-wma-window', 'value'),
                                                       Input('slider-updatemode-wavelet-level', 'value'),
                                                       Input('wavelet-function-radio', 'value'),
                                                       # Input('slider-updatemode-cluster-time-window', 'value'),
                                                       # Input('slider-updatemode-cluster-n-window', 'value')
                                                       ])
    def update_graph_1(plot_radio, subjid_drop, analys_check, slider_value, wavelet_slider_value, wavelet_type):
        subj_id_ = subjid_drop

        if 1 in plot_radio:
            subj_id_ = subj_id_uniques

        # assign data to a new variable name to prevent over-write
        plot_df = data

        if 'wma_denoise' in analys_check:
            """ wma denoising"""

            window_size = slider_value
            weights = np.ones(window_size)

            new_df = pd.DataFrame()
            for i in subj_id_:
                df = pd.DataFrame()
                wma_interim = TClust.wma_df(df=data, i=i, filter_col_name='subjid',
                                            value_col_name='adherence',
                                            weight_func=weights)
                df['adherence'] = wma_interim[2]
                df['subjid'] = [i] * len(wma_interim[2])
                df['datetime'] = wma_interim[1]

                new_df = pd.concat([new_df, df])

            plot_df = new_df

        if 'wd' in analys_check:
            """ wavelet denoising """

            df_ = plot_df.copy(deep=True)
            # df_.fillna(inplace=True, axis=0)
            new_df = pd.DataFrame()
            for i in subj_id_:
                filt_vals = df_[df_['subjid'] == i]
                filt_vals_date = np.array(filt_vals['datetime']).ravel()
                filt_vals_ = np.array(filt_vals['adherence']).ravel()

                data_to_denoise = np.array(filt_vals_).ravel()
                denoised_signal = TClust.wavelet_smooth(data_to_denoise, wavelet=wavelet_type,
                                                        level=wavelet_slider_value)

                if len(denoised_signal) > len(data_to_denoise):
                    print(np.shape(filt_vals_), 'shape of signal to be denoised')
                    print(np.shape(filt_vals_date), 'shape of datetime ')
                    denoised_signal = denoised_signal[:len(data_to_denoise)]

                if len(denoised_signal) < len(data_to_denoise):
                    print(np.shape(filt_vals_), 'shape of signal to be denoised')
                    print(np.shape(filt_vals_date), 'shape of datetime ')
                    filt_vals_date = data_to_denoise[:len(denoised_signal)]

                print(np.shape(denoised_signal), 'shape of denoised signal')

                df = pd.DataFrame()
                df['adherence'] = denoised_signal
                df['subjid'] = [i] * len(denoised_signal)
                df['datetime'] = filt_vals_date

                new_df = pd.concat([new_df, df])

            plot_df = new_df

        if 'tsclust' in analys_check:
            # find a way to work with the plot_df data

            TClust.ts_clustering(df=plot_df, day_start=0, hour_start=0,
                                 hours_window=24 * 4 * 2, colID_subjID='subjid',
                                 colTarget_Att='adherence',
                                 restrict=False, include_dynamic_time_warp=False
                                 )

            pass


        return {
            'data': [
                Scattergl(
                    x=plot_df[plot_df['subjid'] == i]['datetime'],
                    y=plot_df[plot_df['subjid'] == i]['adherence'],
                    # mode='markers',
                    opacity=0.5,
                    marker={
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name=i
                ) for i in subj_id_],
            'layout': Layout(
                title='BYM Data',
                yaxis=dict(title='Adherence Fraction'),
                xaxis=dict(title='Date'))
        }

        # PCAd_data = TClust.ts_clustering(df, day_start, hour_start, colID_subjID,
        #                                  colTarget_Att, hours_window, restrict=False,
        #                                  include_dynamic_time_warp=False)
        #
        # kmeans_c = KMeans(n_clusters=n_clusters).fit(PCAd_data)
        #
        # # define axis
        # X = PCAd_data[:, 0]
        # y = PCAd_data[:, 1]
        #
        # # define grid spacing
        # h = 0.02
        #
        # # Plot the decision boundary. For that, we will assign a color to each
        # x_min, x_max = X.min() - 1, X.max() + 1
        # y_min, y_max = y.min() - 1, y.max() + 1
        # xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        #
        # y_ = np.arange(y_min, y_max, h)
        #
        # print(np.shape(xx), np.shape(yy))
        #
        # # Obtain labels for each point in mesh. Use last trained model.
        # Z = kmeans_c.predict(np.c_[xx.ravel(), yy.ravel()])
        #
        # # Put the result into a color plot
        # Z = Z.reshape(xx.shape)
        #
        # trace1 = Contour(
        #     x=xx[0],
        #     y=y_,
        #     z=Z,
        #     colorscale=[[0, 'purple'],
        #                 [0.5, 'cyan'],
        #                 [1, 'pink']
        #                 ],
        #     opacity=0.5,
        #     showscale=False)
        # trace2 = Scatter(x=X,
        #                  y=y,
        #                  showlegend=False,
        #                  mode='markers',
        #                  marker=dict(color=y,
        #                              line=dict(color='black', width=1)
        #                              ))

        # {
        #     'data': [trace1, trace2],
        #     'layout': Layout(
        #         title='BYM Clustering',
        #         yaxis=dict(title='Adherence Fraction'),
        #         xaxis=dict(title='Date'))
        # }


if __name__ == '__main__':

    # initialize app class
    app = dash.Dash()

    # class instantiation
    TClust = tsCluster()

    # init plotly offline
    init_notebook_mode(connected=True)

    # read in the data
    df = pd.read_csv(TClust.TimeSeriesVert)

    # correct the time series to provide a single timestamp
    df = TClust.hours_column2timestamp(df)

    # drop the data nans
    data = df.copy(deep=True)

    cluster_data = df.copy(deep=True)

    data.drop(['hour', 'date', 'visit'], inplace=True, axis=1)

    print(np.shape(data))
    data.dropna(inplace=True, axis=0)
    print(np.shape(data))

    # remove subjid's with less than 10 values
    g = data.groupby('subjid')
    data = g.filter(lambda x: len(x) > 10)

    subj_id_uniques = np.unique(data['subjid'])
    subj_id_uniques = list(map(int, subj_id_uniques))

    # isolate individual subjects data
    subj_id = subj_id_uniques[0]
    subj_data = data[data['subjid'] == subj_id]

    # Create list of subject IDs for dropdown
    subjDropdownDictList = list()
    for subj_id in subj_id_uniques:
        subjDropdownDictListInt = dict()
        subjDropdownDictListInt['label'] = str(subj_id)
        subjDropdownDictListInt['value'] = subj_id
        subjDropdownDictList.append(subjDropdownDictListInt)

    subjDescribe = pd.DataFrame()
    for subj_id in subj_id_uniques:
        desc = data[data['subjid'] == subj_id].describe()
        desc_out = pd.concat([subjDescribe, desc])

    app_call()

    app.run_server(debug=True)
