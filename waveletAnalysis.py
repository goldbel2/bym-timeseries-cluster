import pywt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tsCluster import tsCluster

# class instantiation
TClust = tsCluster()

# read in the data
df = pd.read_csv(TClust.TimeSeriesVert)

# correct the time series to provide a single timestamp
df = TClust.hours_column2timestamp(df)

# filter by subject, select single subject
subject = 1001002
subj_df = df.loc[df["subjid"] == subject]

# level 4 db2 frequency symetric wavelet 'scaleogram'
scaleogram = True
if scaleogram:
    # drop not useful columns
    subj_df = subj_df.drop(['subjid', 'date', 'hour', 'visit', 'date'], axis=1)
    subj_df = subj_df.dropna()

    x = subj_df['datetime']
    data = subj_df['adherence']

    wavelet = 'db2'
    level = 4
    order = "freq"  # other option is "normal"
    interpolation = 'nearest'
    cmap = plt.cm.cool

    # Construct wavelet packet
    wp = pywt.WaveletPacket(data, wavelet, 'symmetric', maxlevel=level)
    nodes = wp.get_level(level, order=order)
    labels = [n.path for n in nodes]
    values = np.array([n.data for n in nodes], 'd')
    values = abs(values)

    # Show signal and wavelet packet coefficients
    fig = plt.figure()
    fig.subplots_adjust(hspace=0.2, bottom=.03, left=.07, right=.97, top=.92)
    ax = fig.add_subplot(2, 1, 1)
    ax.set_title("linchirp signal")
    ax.plot(x, data, 'b')
    # ax.set_xlim(0, x[-1])

    ax = fig.add_subplot(2, 1, 2)
    ax.set_title("Wavelet packet coefficients at level %d" % level)
    ax.imshow(values, interpolation=interpolation, cmap=cmap, aspect="auto",
              origin="lower", extent=[0, 1, 0, len(values)])
    ax.set_yticks(np.arange(0.5, len(labels) + 0.5), labels)

    # Show spectrogram and wavelet packet coefficients
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(211)
    ax2.specgram(data, NFFT=64, noverlap=32, Fs=2, cmap=cmap,
                 interpolation='bilinear')
    ax2.set_title("Spectrogram of signal")
    ax3 = fig2.add_subplot(212)
    ax3.imshow(values, origin='upper', extent=[-1, 1, -1, 1],
               interpolation='nearest')
    ax3.set_title("Wavelet packet coefficients")

    plt.show()

#
# import pywt.data
# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# import numpy as np
# import matplotlib.pyplot as plt
#
#
# import pywt.data
#
#
# # Load image
# original = pywt.data.aero()
#
# # Wavelet transform of image, and plot approximation and details
# titles = ['Approximation', ' Horizontal detail',
#           'Vertical detail', 'Diagonal detail']
# coeffs2 = pywt.dwt2(original, 'bior1.3')
# LL, (LH, HL, HH) = coeffs2
# fig = plt.figure()
# for i, a in enumerate([LL, LH, HL, HH]):
#     ax = fig.add_subplot(2, 2, i + 1)
#     ax.imshow(a, origin='image', interpolation="nearest", cmap=plt.cm.gray)
#     ax.set_title(titles[i], fontsize=12)
#
# fig.suptitle("dwt2 coefficients", fontsize=14)
#
# # Now reconstruct and plot the original image
# reconstructed = pywt.idwt2(coeffs2, 'bior1.3')
# fig = plt.figure()
# plt.imshow(reconstructed, interpolation="nearest", cmap=plt.cm.gray)
#
# # Check that reconstructed image is close to the original
# np.testing.assert_allclose(original, reconstructed, atol=1e-13, rtol=1e-13)
#
#
# # Now do the same with dwtn/idwtn, to show the difference in their signatures
#
# coeffsn = pywt.dwtn(original, 'bior1.3')
# fig = plt.figure()
# for i, key in enumerate(['aa', 'ad', 'da', 'dd']):
#     ax = fig.add_subplot(2, 2, i + 1)
#     ax.imshow(coeffsn[key], origin='image', interpolation="nearest",
#               cmap=plt.cm.gray)
#     ax.set_title(titles[i], fontsize=12)
#
# fig.suptitle("dwtn coefficients", fontsize=14)

# Now reconstruct and plot the original image
reconstructed = pywt.idwtn(coeffsn, 'bior1.3')
fig = plt.figure()
plt.imshow(reconstructed, interpolation="nearest", cmap=plt.cm.gray)

# Check that reconstructed image is close to the original
np.testing.assert_allclose(original, reconstructed, atol=1e-13, rtol=1e-13)


plt.show()