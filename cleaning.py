import pandas as pd
import datetime
import numpy as np
import os, glob
import matplotlib.pyplot as plt
from scipy.spatial.distance import euclidean
import matplotlib as mpl
from fastdtw import fastdtw
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from tsCluster import tsCluster
from dtaidistance import dtw
from statsmodels.robust import mad
import pywt


if __name__ == "__main__":
    # class instantiation
    tsCluster = tsCluster()

    # read in the data
    data = pd.read_csv(tsCluster.TimeSeriesVert)

    # correct the time series to provide a single timestamp
    data = tsCluster.hours_column2timestamp(data)


    def waveletSmooth(x, wavelet="coif1", level=1):
        # calculate the wavelet coefficients
        coeff = pywt.wavedec(x, wavelet, mode="per",axis=0)

        # calculate a threshold
        sigma = mad(coeff[-level])

        uthresh = sigma * np.sqrt(1 * np.log(len(x)))
        coeff[1:] = (pywt.threshold(i, value=uthresh, mode="soft") for i in coeff[1:])

        # reconstruct the signal using the thresholded coefficients
        return pywt.waverec(coeff, wavelet, mode="per")

    base_data = data['adherence']
    print(np.shape(base_data))
    base_data.dropna(inplace=True)
    print(np.shape(base_data))
    # base_data = [1,2,3,4,5,6,7,8]
    data_to_denoise = np.array(base_data.copy())
    denoised_data = waveletSmooth(data_to_denoise)
    print(np.shape(data_to_denoise))
    print(np.shape(denoised_data))
    plt.plot(data_to_denoise)
    plt.plot(denoised_data)
    plt.show()




    # add weekdays
    data = tsCluster.add_weekday(data)

    # align the subject to Monday at 00:00; store output .csvs
    tsCluster.align_by_day_hour(data, day_start=0, hour_start=0)

    # align the series by time for target attribute
    data = tsCluster.align_series(data,
                                  colID_subjID='subjid',
                                  colTarget_Att='adherence')

    subject_id_header = list(data)
    print(subject_id_header)

    # define hours to split and stack
    # hours_window = np.shape(data)
    hours_window = 24 * 7   # 4 months

    split_stacked_aligned = tsCluster.split_stack_by_hours(data, hours_window)
    print('Shape pre-zero empty drop', np.shape(split_stacked_aligned))

    subdivision, hours = np.shape(split_stacked_aligned)


    # bring into a dataframe to easily drop 0s
    split_stacked_aligned_df = pd.DataFrame(split_stacked_aligned)
    split_stacked_aligned = split_stacked_aligned_df[(split_stacked_aligned_df.T != 0).any()]
    print('Shape post zero empty drop', np.shape(split_stacked_aligned))

    # calculate mean compliance by time definition
    split_stacked_aligned_mean = np.ravel(split_stacked_aligned.mean(axis=1))
    print('Length of stacked aligned mean', len(split_stacked_aligned_mean))

    # find dynamic time warp distance metric (returns as matrix)
    ## compare the distance to the mean to bin classes.
    restrict = False  # np.shape(split_stacked_aligned)[0]

    if restrict:
        split_stacked_aligned = split_stacked_aligned[:int(restrict)]

    include_dynamic_time_warp = False

    if include_dynamic_time_warp:
        dtw_dist_mat = tsCluster.dynamic_time_warp_dist(split_stacked_aligned,
                                                        restict=restrict,
                                                        signal_comparator=split_stacked_aligned_mean)

    dtws = dtw.distance_matrix_fast(data)
    print(dtws)

    # combine dtw distance metric with other metrics, like PCA
    # add the dtw distance metrics to the the PCA results - don't worry, this is only one way
    split_stacked_aligned_PCA_dtw_train = split_stacked_aligned

    # split_stacked_aligned_PCA_dtw_train = np.c_[split_stacked_aligned, dtw_dist_mat]
    print(np.shape(split_stacked_aligned_PCA_dtw_train))

    ## calculate PCA for the raw split/stacked compliance data
    n_components = 2  # only take first two components
    split_stacked_aligned_PCA = PCA(n_components=n_components).fit_transform(split_stacked_aligned_PCA_dtw_train)
    print(np.shape(split_stacked_aligned_PCA_dtw_train))

    # define axis
    X = split_stacked_aligned_PCA[:, 0]
    y = split_stacked_aligned_PCA[:, 1]

    plot_decision_boundary = True

    if plot_decision_boundary:
        tsCluster.plot_decision_boundary(X=X, y=y, PCAd_data=split_stacked_aligned_PCA)

    # train and fit an unsupervised clustering method
    # for now, no test/validation
    n_clusters = 3
    kmeans_c = KMeans(n_clusters=n_clusters).fit(split_stacked_aligned_PCA)
    cluster_classes = np.unique(kmeans_c.labels_)
    tag = kmeans_c.labels_
    predicted_classes = kmeans_c.predict(split_stacked_aligned_PCA)


    # create a dataframe that stores the data and class for averaging
    ts_clustered_df = split_stacked_aligned.copy(deep=True)
    if restrict:
        ts_clustered_df = ts_clustered_df[:restrict]

    ts_clustered_df['class'] = np.ravel(predicted_classes)

    # # find averages and plot for each class
    df_filt_mat_means = []
    i = 0
    for cluster in cluster_classes:
        df_filt = ts_clustered_df.copy(deep=True)
        df_filt = df_filt[df_filt['class'] == i]
        df_filt.drop(['class'], inplace=True, axis=1)
        df_filt_mat_means.append(np.ravel(df_filt.mean()))
        i += 1

    # plot
    # simply generate repeatable color mapping
    # label_color_map = {0: 'r', 1: 'k', 2: 'b', 3: 'g', 0: 'y'}
    label_color_map = {0: (166,207,227), 1: (252,191,109), 2: (179,91,41), 3: 'g', 0: 'y'}

    # the time series are stored in split_stacked_aligned
    split_stacked_aligned_mat = np.matrix(split_stacked_aligned)
    i = 0
    for week in range(0, restrict):
        plt.plot(np.ravel(split_stacked_aligned_mat[week, :]),
                 color=label_color_map.get(predicted_classes[i]),
                 alpha=0.3)
        i += 1

    i = 0
    plt.show()

    for cluster in cluster_classes:
        plt.plot(df_filt_mat_means[i],
                 color=label_color_map.get(label_color_map.get(i)),
                 alpha=1, label=str('Compliance average for cluster class: ' + str(i)))
        i += 1
    plt.legend()
    plt.title('Cluster-assigned average compliance patterns\n'
              'on the BYM dataset')
    plt.show()




    # subjects should be in the stud for 107 days. Innately different days, may
    # cluster by week (10 weeks/20 weeks from the same patient).
    # Add the subject ID into the clustering matrix
    # Seperate by subjectID, you would see the clusters dominated by subject.
    # why do they cluster? They cluster because of visits
    # setup a relationship between classes and vists
    # relationship between clusters and subjects, and sites
    # identify clusters, characterize clusters, use this info to a better subsampling method (parametric).
    # clustering at different time scales. Crude characterization of clusters to get some explanation.















#
#
# # read in data

#
# # change date to the right format (datetime64)
#
#
# # modify to the real timestamp
# data['datetime'] = data['date'] + pd.to_timedelta(data['hour'], unit='h')
# data['weekday'] = data['datetime'].dt.dayofweek
#
# #
# align_subj_to_weekday_hour_start(data, 1, 0)
#
# start_date_to_subj_week_hour_aligned = combine_ts(raw_all_batch=FilePaths.TimeSeriesStore)
# start_date_to_subj_week_hour_aligned.to_csv('aligned_start_dow.csv')
#
# aligned_series = align_series(data=start_date_to_subj_week_hour_aligned)
#
# aligned_series['average'] = aligned_series.mean(axis=1)
#
# window_size = 12
# weights = np.ones(window_size)
#
# aligned_series['wma'] = weighted_moving_average(aligned_series['average'], weights)
#
# # there are 168 hours in 7 days
# # aligned_series['average'].plot()
# # aligned_series['wma'].plot()
# # #
# # print(aligned_series.head())
# # plt.show()
#
# # split the dataframe into series of 168 hours.
# patient_data_by_week = []
# mat_df = []
# dff = pd.DataFrame()
# for g, df in aligned_series.groupby(np.arange(len(aligned_series)) // 168):
#     df = df.transpose()
#
#     if g < 33:
#         mat_df.append(np.matrix(df))
#
# # vertically stack the data to get the weekly shapes.
# vstackNP = np.vstack(mat_df)
# all_weeks_compliance = pd.DataFrame(vstackNP.transpose())
#
# all_weeks_compliance_mean = all_weeks_compliance.mean(axis=1)
# print(all_weeks_compliance_mean)
#
# # fig, ax = plt.subplots()
# # ax.plot(all_weeks_compliance_mean)
# # ax.xaxis.set_ticks(np.arange(0, 168+24, 24))
# # plt.show()
#
# # now let's look at the LB_Keogh distance between the average and each time series.
# ## shape of the vertically stacked and transposed weekly compliance
# ts_hours, ts_weeks = np.shape(all_weeks_compliance)
# # ts_weeks = 300
# all_weeks_compliance = np.matrix(all_weeks_compliance).transpose()
# print(np.shape(all_weeks_compliance))
# LB_Keogh_dist = []
# LB_Keogh_path = []
# for week in range(0, ts_weeks):
#     distance, path = fastdtw(np.ravel(all_weeks_compliance[week, :]), all_weeks_compliance_mean, dist=euclidean)
#     LB_Keogh_dist.append(distance)
#     LB_Keogh_path.append(path)
#
#
# # X = PCA(n_components=2).fit_transform(all_weeks_compliance)[:int(ts_weeks)]
# X = PCA(n_components=2).fit_transform(all_weeks_compliance)[:int(ts_weeks)]
# print(np.shape(X), np.shape(LB_Keogh_dist))
#
# X_dist_pca_train = np.c_[X, LB_Keogh_dist]
#
# # X_dist_pca_train = np.c_[X[:int(ts_weeks/2)], LB_Keogh_dist[:int(ts_weeks/2)]]
# # X_dist_pca_test = np.c_[X[int(ts_weeks/2+1):], LB_Keogh_dist[int(ts_weeks/2+1):]]
#
# # X_dist_pca_train = np.c_[LB_Keogh_dist[:int(ts_weeks/2)], LB_Keogh_path[:int(ts_weeks/2)]]
# # X_dist_pca_test = np.c_[LB_Keogh_dist[int(ts_weeks/2+1):], LB_Keogh_path[int(ts_weeks/2+1):]]
#
#
# kmeans_c = KMeans(n_clusters=4).fit(X_dist_pca_train)
# km_clust = pd.DataFrame(data=kmeans_c.labels_, columns=['cluster_assignment'])
#
# # isolate unique cluster assignments
# cluster_classes = np.unique(np.array(km_clust['cluster_assignment']))
# #
# y_pred = kmeans_c.predict(X_dist_pca_train)
#
# all_weeks_compliance_df = pd.DataFrame(data=all_weeks_compliance)
# all_weeks_compliance_df = all_weeks_compliance_df[:ts_weeks]
#
# label_color_map = {0: 'r', 1: 'k', 2: 'b', 3: 'g'}
# label_color = [label_color_map[l] for l in kmeans_c.labels_]
#
# # print(aligned_series.head())
# # print(all_weeks_compliance_df.head())
# # df = all_weeks_compliance_df.iloc[1:3,:]
# # df= df.T
# # df.plot()
# # plt.plot(all_weeks_compliance_mean,'r')
# # print(all_weeks_compliance)
# # plt.show()
#
# # create a dataframe with week data and plot color for ease
# # print(len(kmeans_c.labels_), 'label length', len(all_weeks_compliance),'all week comp length')
# all_weeks_compliance_df['class'] = np.ravel(kmeans_c.labels_)
# print(all_weeks_compliance_df.head())
#
# # just the first class
#
# df_c1 = all_weeks_compliance_df[all_weeks_compliance_df['class'] == 0]
# df_c2 = all_weeks_compliance_df[all_weeks_compliance_df['class'] == 1]
# df_c3 = all_weeks_compliance_df[all_weeks_compliance_df['class'] == 2]
# df_c4 = all_weeks_compliance_df[all_weeks_compliance_df['class'] == 3]
#
# df_c1.drop(['class'], inplace=True, axis=1)
# df_c2.drop(['class'], inplace=True, axis=1)
# df_c3.drop(['class'], inplace=True, axis=1)
# df_c4.drop(['class'], inplace=True, axis=1)
#
# plt.plot(df_c1.mean())
# plt.plot(df_c2.mean())
# plt.plot(df_c3.mean())
# plt.plot(df_c4.mean())
# plt.show()


#
#
# i =0
# for week in range(0, ts_weeks):
#     plt.plot(np.ravel(all_weeks_compliance[week,:]),color=label_color_map.get(y_pred[i]), alpha = 0.3)
#     i +=1
#
# # plt.scatter(x=X_dist_pca_test[:,0],y=X_dist_pca_test[:,2],c=label_color, alpha = 0.5)
# plt.show()
